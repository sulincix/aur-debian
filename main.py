#!/usr/bin/python3
import os, sys
import subprocess
os.environ["LANG"]="C"

def is_pkg_valid(name):
    out = subprocess.getoutput("apt-cache policy '{}'".format(name))
    return "Candidate:" in out and "(none)" not in out

def is_pkg_installed(name):
    out = subprocess.getoutput("apt-cache policy '{}'".format(name))
    return "Installed:" not in out

def pkg_conflicts(name):
    out = subprocess.getoutput("apt show {}".format(name))
    for line in out.split("\n"):
        if "Conflicts" in line:
            return line.split(": ")[1].split(", ")
    return []

def run(cmd,vital=True):
    print("\x1b[32;1mRunning: \x1b[;0m{}".format(cmd))
    i = os.system(cmd)
    if i != 0 and vital:
        sys.exit(i)
    return i

def get_pkg_from_source(sources=[]):
    pkgs = []
    conflict = []
    for src in sources:
        #if is_pkg_valid(src):
        #    pkgs.append(src)
        #    continue
        output = subprocess.getoutput("apt-cache showsrc {}".format(src))
        e = False
        for line in output.split("\n"):
            if line.startswith("Package-List:"):
                e = True
                continue
            if line.startswith("Directory:"):
                e = False
                continue
            if e:
                pkg = line.strip().split(" ")[0]
                if is_pkg_valid(pkg):
                    conflict += pkg_conflicts(pkg)
                    if pkg not in conflict:
                        pkgs.append(pkg)
                if is_pkg_valid(pkg+"-dev"):
                    conflict += pkg_conflicts(pkg+"-dev")
                    if pkg+"-dev" not in conflict:
                        pkgs.append(pkg+"-dev")
    return pkgs

def clone_aur_repo(name=""):
    if not os.path.isdir(name):
        run("git clone https://aur.archlinux.org/{0}.git {0}".format(name))

def fetch_arch_pkg(name=""):
    run("mkdir -p .tmp")
    if 0 != run("wget 'https://github.com/archlinux/svntogit-community/archive/refs/heads/packages/{}.zip' -O .tmp/pkg.zip".format(name),vital=False):
        run("wget 'https://github.com/archlinux/svntogit-packages/archive/refs/heads/packages/{}.zip' -O .tmp/pkg.zip".format(name))
    run("cd .tmp && unzip pkg.zip")
    run("mv .tmp/*/trunk ./'{}'".format(name))
    run("rm -rf .tmp")


def read_pkgbuild_variable(var="",array=False):
    if array:
        return subprocess.getoutput("bash -c 'source PKGBUILD ; echo ${"+var+"[@]}'").strip().split(" ")
    return subprocess.getoutput("bash -c 'source PKGBUILD ; echo ${"+var+"}'").strip()

def define_environ():
    _env = os.environ.copy()
    os.environ.clear()
    os.environ["PATH"] = "/bin:/sbin:/usr/bin:/usr/sbin"
    os.environ["pkgdir"] = os.getcwd()+"/install"
    os.environ["srcdir"] = os.getcwd()

def run_function(func=""):
    define_environ()
    pkgapi = "/lib/pkgbuild-api.sh"
    if os.path.exists("../pkgbuild-api.sh"):
        pkgapi = "../pkgbuild-api.sh"
    if os.system("bash -c 'source PKGBUILD ; [[ $(type -t {}) == function ]]'".format(func)) == 0:
        print("\x1b[32;1mRunning: \x1b[;0m{}".format(func))
        i = os.system("exec bash -c 'set -e ; source {0} ; source PKGBUILD ; {1}'".format(pkgapi,func))
        if i != 0:
            print("\x1b[32;1mFailed to run {}: \x1b[;0m{}".format(func,i))
            sys.exit(i)

def arch_pkgname_convert(names=[]):
    ret = []
    for name in names:
        if ">=" in name:
            name = name.split(">=")[0]
        if name.startswith("python-"):
            name = name.replace("python-","python3-")
        if name.startswith("python2-"):
            name = name.replace("python2-","python-")
        if name.endswith(".so") or name in ["glibc", "git"]:
            continue
        ret.append(name)
        if is_pkg_valid(name+"-dev"):
            ret.append(name+"-dev")
    return ret

def install_package(names=[]):
    if len(names) == 0:
        return True
    cmd = "apt install -yq "
    for name in names:
        if not is_pkg_valid(name):
            continue
        if not is_pkg_installed(name):
            cmd += name + " "
    return run(cmd) == 0

def fetch_source(src):
    target = None
    if "#tag=" in src:
        # FIXME: remove this
        src = src.split("#tag=")[0]
        
    if src.startswith("git+"):
        return run("git clone '{}'".format(src[4:]))
    elif "::" in src:
        target = src.split("::")[0]
        if os.path.isdir(target):
            return
        src = src.split("::")[1]
        if src.startswith("git+"):
            run("git clone '{}' '{}'".format(src[4:],target))
        else:
            run("wget -c '{}' -O '{}'".format(src,target))
    elif "git://" in src:
        return run("git clone '{}'".format(src))
    elif "://" in src:
        run("wget -c '{}'".format(src))
    if not target:
        target = os.path.basename(src)
    if src.endswith("tar.bz2") or src.endswith("tar.xz") or src.endswith("tar.gz") or src.endswith("tar.zst"):
        run("tar -xf {}".format(target))
    elif src.endswith("zip"):
        run("unzip {}".format(target))
    elif src.endswith("deb"):
        run("ar x {}".format(target))
    elif src.endswith("rpm"):
        run("rpm2cpio {} | cpio -id".format(target))

def write_control(pkgdeps):
    control = open("install/DEBIAN/control","w")
    deps = arch_pkgname_convert(pkgdeps)
    deps = get_pkg_from_source(deps)
    def w(section, value):
        control.write("{}: {}\n".format(section,read_pkgbuild_variable(value)))
    w("Package","pkgname")
    control.write("Version: {}\n".format(mkversion(read_pkgbuild_variable("pkgver"))))
    control.write("Depends: ")
    for dep in deps:
        if len(dep.strip()) > 0:
            control.write("{}, ".format(dep))
    control.write("libc6\n")
    control.write("Priority: optional\n")
    control.write("Maintainer: Aur builder <aur@archlinux.org>\n")
    control.write("Architecture: amd64\n")
    w("Homepage","url")
    w("Description","pkgdesc")
    control.write(" This package generated from aur repository.\n")
    control.close()

def mkversion(ver):
    ret = ""
    for c in ver:
        if c in ["0","1","2","3","4","5","6","7","8","9","-","."]:
            ret +=c
    return ret

def build_remote(names=[]):
    curdir = os.getcwd()
    for name in names:
        if os.path.exists(name):
            os.system("rm -rf '{}'".format(name))
        if "--archlinux" in sys.argv:
            fetch_arch_pkg(name)
        else:
            clone_aur_repo(name)
        os.chdir(name)
        build()
        os.chdir(curdir)

def build():
    deps = []
    print("\x1b[32;1mResolving dependencies: \x1b[;0m phase-1")
    pkgdeps = read_pkgbuild_variable("depends",True)
    deps += pkgdeps
    mkdeps = read_pkgbuild_variable("makedepends",True)
    deps += mkdeps
    print("\x1b[32;1mResolving dependencies: \x1b[;0m phase-2")
    deps = arch_pkgname_convert(deps)
    deps = get_pkg_from_source(deps)
    install_package(deps)
    for src in read_pkgbuild_variable("source",True):
        fetch_source(src)
    for src in read_pkgbuild_variable("source_x86_64",True):
        fetch_source(src)
    if not os.path.isdir("install/DEBIAN"):
        os.mkdir("install")
        os.mkdir("install/DEBIAN")
    run_function("prepare")
    run_function("build")
    run_function("package")
    write_control(pkgdeps)
    run("dpkg -b install")
    if "-i" in sys.argv or input("Do you want to install package?\n=>").lower() == "y":
        run("apt install -f ./install.deb")

if os.path.exists("PKGBUILD"):
    build()
for pkg in sys.argv[1:]:
    if not pkg.startswith("-"):
        print("Building:"+pkg)
        build_remote([pkg])

